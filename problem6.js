function bmwAndAudi(inventory){
    let cars=[];
    if(!inventory || !Array.isArray(inventory) || inventory.length===0)
    return [];
    for(let i=0;i<inventory.length;i++){
        if(inventory[i].car_make==='Audi' || inventory[i].car_make==='BMW'){
            cars.push(inventory[i]);
        }
    }
    const carjson=JSON.stringify(cars);
    return carjson;
}

module.exports = bmwAndAudi;

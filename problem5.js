function carYears(inventory){
    let oldCars=[];
    let count=0;
    if(!inventory || inventory.length===0 || !Array.isArray(inventory))
        return [];
    for(let i=0;i<inventory.length;i++){
        if(inventory[i].car_year<2000){
            oldCars.push(inventory[i].car_year);
            count++;
        }
    }
    console.log(`Total number of Old Cars : ${count}`);
    return oldCars;
}

module.exports = carYears;

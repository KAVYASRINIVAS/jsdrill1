function findLastCar(carData=undefined){
    if(!carData || carData.length===0 || !Array.isArray(carData))
        return [];
    else{
        const i=carData.length-1;
        return carData[i];
        //return (`Last Car is a ${carData[i].car_make} ${carData[i].car_model}`);
    }
    
}

module.exports = findLastCar;
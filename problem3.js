function sortModel(inventory){
    if(!inventory || inventory.length===0 || !Array.isArray(inventory)){
        return [];
    }
    for(let i=0;i<inventory.length-1;i++){
        for(let j=i+1;j<inventory.length;j++){
        if(inventory[i].car_model>inventory[j].car_model){
            let temp=inventory[i];
            inventory[i]=inventory[j];
            inventory[j]=temp;
        }
    }
    }
    return inventory;
}

module.exports = sortModel;

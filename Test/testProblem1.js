const carData=require("../cars.js");
const testProb1=require("../problem1");


const test1 = testProb1(carData,33);
const test2 = testProb1(carData);
const test3 = testProb1(carData,'a');
const test4 = testProb1(6,5);
const test5 = testProb1(carData,25);
const test6 = testProb1(carData,55);

console.log(test1);
console.log(test2);
console.log(test3);
console.log(test4);
console.log(test5);
console.log(test6);
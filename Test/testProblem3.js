const carData=require("../cars.js");
const testProb3=require("../problem3.js");

const test1=testProb3(carData);
console.log(test1);

//The below input returns an empty array []
const test2=testProb3([]);
console.log(test2);

//The below input returns an empty array []
const test3=testProb3('hello');
console.log(test3);